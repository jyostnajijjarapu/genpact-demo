import React, { useState } from 'react'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar'
import { CreateTabs, ImagesData } from './CreateTabs'
// import { ImagesData } from './CreateTabs'
import { useNavigate } from "react-router-dom"
import './createInstance.css'
import { useLocation } from 'react-router-dom'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Typography from '@mui/material/Typography';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import PropTypes from 'prop-types';

import Details from '../../components/Details/Details';
import Security from '../../components/Security/Security';
import Networking from '../../components/Networking/Networking';
import Storage from '../../components/Storage/Storage';
import Tags from '../../components/Tags/Tags';
import Advanced from '../../components/Advanced/Advanced';
export default function CreateInstance() {
    const location = useLocation();
    console.log(location.state, "ppppppppppp")

    const navigate = useNavigate()

    const onSubmitHandler = () => {

    }
    const onCreateHandler = () => {
        navigate('/dashboard', { state: isDisabled })

        // setDisabled({ isDisabled: 'IN' })
    }
    const onNextHandler = () => {

    }
    const [isDisabled, setDisabled] = React.useState('IN')
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    function CustomTabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                role="tabpanel"
                hidden={value !== index}
                id={`simple-tabpanel-${index}`}
                aria-labelledby={`simple-tab-${index}`}
                {...other}
            >
                {value === index && (
                    // <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                    // </Box>
                )}
            </div>
        );
    }

    CustomTabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.number.isRequired,
        value: PropTypes.number.isRequired,
    };
    function a11yProps(index) {
        return {
            id: `simple-tab-${index}`,
            'aria-controls': `simple-tabpanel-${index}`,
        };
    }
    return (
        <>
            <Topbar />
            <div className="homeContainer">
                <Sidebar />
                <div className='content'>
                    <h1 style={{ color: 'GrayText' }}>Create Instance</h1>
                    <div className='contentCard'>


                        <div>
                            {/* {CreateTabs.map((val, key) => {
                                return (
                                    <button className='tabs'
                                        id={val.link == 'details' ? "active" : ""}
                                        key={key}
                                        onClick={() => navigate(val.link)}>{val.tab}
                                    </button>
                                )
                            })
                            } */}

                            <Tabs value={value} onChange={handleChange} aria-label="Ec2 tabs">
                                <Tab label="Details" {...a11yProps(0)} />
                                <Tab label="Security" {...a11yProps(1)} />
                                <Tab label="Networking" {...a11yProps(2)} />
                                <Tab label="Storage" {...a11yProps(3)} />
                                <Tab label="Tags" {...a11yProps(4)} />
                                <Tab label="Advanced" {...a11yProps(5)} />
                            </Tabs>
                            <CustomTabPanel value={value} index={0}>
                                <Details />
                            </CustomTabPanel>
                            <CustomTabPanel value={value} index={1}>
                                <Security />
                            </CustomTabPanel>
                            <CustomTabPanel value={value} index={2}>
                                <Networking />
                            </CustomTabPanel>
                            <CustomTabPanel value={value} index={3}>
                                <Storage />
                            </CustomTabPanel>
                            <CustomTabPanel value={value} index={4}>
                                <Tags />
                            </CustomTabPanel>
                            <CustomTabPanel value={value} index={5}>
                                <Advanced />
                            </CustomTabPanel>
                        </div>

                    </div>
                    <div style={{
                        display: 'flex', justifyContent: 'flex-end', marginBottom: '10px'
                    }}>
                        <button className='previousBtn'  > <ChevronLeftIcon fontSize='small' />Previous</button>
                        <button className='nextBtn' onClick={onNextHandler}  >Next: Security <ChevronRightIcon fontSize='small' /> </button>
                    </div>

                    {/* <div>
                        <button className='submitBtn' onClick={onCreateHandler} >Create</button>
                        <button className='submitBtn' onClick={onSubmitHandler} disabled={location.state === 'IN' ? false : true} >Submit</button>
                    </div> */}
                </div>


            </div>
        </>
    )
}
