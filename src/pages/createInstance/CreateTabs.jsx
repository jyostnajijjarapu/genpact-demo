export const CreateTabs = [
    {
        tab: 'Details',
        link: 'details'
    },
    {
        tab: 'Security',
        link: 'security'
    },
    {
        tab: 'Networking',
        link: 'networking'
    },
    {
        tab: 'Storage',
        link: 'storage'
    },
    {
        tab: 'Tags',
        link: 'tags'
    },
    {
        tab: 'Advanced',
        link: 'advanced'
    },
];
