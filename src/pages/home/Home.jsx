import React from 'react'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Rightbar from '../../components/rightbar/Rightbar'
import './home.css'
import { useLocation } from 'react-router-dom'
export default function Home(props) {
    // const location = useLocation();
    // console.log(location.state.user, "home")


    return (
        <>
            <Topbar />
            <div className="homeContainer">
                <Sidebar />

                <Rightbar />
                {/* username={location.state.user} */}
            </div>

        </>

    )
}
