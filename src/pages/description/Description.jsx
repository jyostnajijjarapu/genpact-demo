import React, { useState } from 'react'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar';
import './description.css'
import TableRows from "../../components/tableRows/TableRows";

export default function Description() {
    const [rowsData, setRowsData] = useState([]);

    const addTableRows = () => {

        const rowsInput = {
            fullName: '',
            emailAddress: '',
            salary: ''
        }
        setRowsData([...rowsData, rowsInput])

    }
    const deleteTableRows = (index) => {
        const rows = [...rowsData];
        rows.splice(index, 1);
        setRowsData(rows);
    }

    const handleChange = (index, evnt) => {

        const { name, value } = evnt.target;
        const rowsInput = [...rowsData];
        rowsInput[index][name] = value;
        setRowsData(rowsInput);



    }
    return (
        <>
            <Topbar />
            <div className="homeContainer">
                <Sidebar />
                {/* <div className='content'>
                    <h3>This is the L3 User Approval page</h3>
                    <button className='acceptBtn' onClick={onAcceptHandler}>Accept</button>
                    <button className='rejectBtn' onClick={onRejectHandler}>reject</button>
                </div> */}
                <div className="reportView">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Email Address</th>
                                            <th>Salary</th>
                                            <th><button className="btn btn-outline-success" onClick={addTableRows} >+</button></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <TableRows rowsData={rowsData} deleteTableRows={deleteTableRows} handleChange={handleChange} />
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-sm-4">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>

    )
}
