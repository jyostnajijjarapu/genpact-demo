import React, { useState } from 'react'
import './login.css'
import { useNavigate } from "react-router-dom"
export default function Login() {
    const navigate = useNavigate()
    const [user, setUser] = React.useState('L1 User')

    const onClickHandler = () => {
        // setData
        console.log(user, "login")
        localStorage.setItem('user', user)
        navigate('/home')
    }
    const handleChange = (userrole) => {
        setUser(userrole)
        // console.log(userrole, "login")
    }

    return (
        <div className='loginContainer'>
            <div className='login'>
                <div>
                    <label>Select User Role:</label>
                    <select value={user} onChange={(event) => handleChange(event.target.value)}>
                        <option value='L1 User'>L1 User</option>
                        <option value='L2 User'>L2 User</option>
                        <option value='L3 User'>L3 User</option>
                    </select>
                </div>
                <div>
                    <button className='loginBtn' onClick={onClickHandler} >Login</button>

                </div>
            </div>
        </div>
    )
}
