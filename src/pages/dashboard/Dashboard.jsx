import React from 'react'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Rightbar from '../../components/rightbar/Rightbar'
import './dashboard.css'
import { useNavigate } from "react-router-dom"
import { useLocation } from 'react-router-dom'

export default function Dashboard(props) {
    const location = useLocation();
    // console.log(location.state, "kkkkkkkkkkkkk")
    const navigate = useNavigate()

    const onAcceptHandler = () => {
        navigate('/createInstance', { state: 'out' })
        localStorage.setItem('submit', true)
    }
    const onRejectHandler = () => {
        navigate('/createInstance', { state: 'IN' })
        localStorage.setItem('submit', false)
    }
    // generateSafeImageUrl(): SafeUrl {
    //     return this.sanitizer.bypassSecurityTrustResourceUrl(this.reportURL);
    // }
    return (
        <>
            <Topbar />
            <div className="homeContainer">
                <Sidebar />
                {/* <div className='content'>
                    <h3>This is the L3 User Approval page</h3>
                    <button className='acceptBtn' onClick={onAcceptHandler}>Accept</button>
                    <button className='rejectBtn' onClick={onRejectHandler}>reject</button>
                </div> */}
                <div className="reportView">
                    <iframe className="reportFrameView" src="https://app.powerbi.com/view?r=eyJrIjoiNWNiN2ZjOGItNTU2My00ZjAxLTk1NjEtZmFlN2RmNDFmODk5IiwidCI6IjQzMmE0MjE5LTFhNDYtNGI3Zi05MmNlLWFhZTdiYzcwNWMyNiJ9"></iframe>
                </div>


            </div>
        </>
    )
}
