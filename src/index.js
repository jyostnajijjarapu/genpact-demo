import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Home from './pages/home/Home';
// import Login from './pages/login/Login';
import Dashboard from './pages/dashboard/Dashboard';
import Description from './pages/description/Description';
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Link,
} from "react-router-dom";
import CreateInstance from './pages/createInstance/CreateInstance'
const router = createBrowserRouter([

  {
    path: "/",
    element: <App />
  },
  {
    path: "home",
    element: <Home />
  },
  {
    path: "dashboard",
    element: <Dashboard />
  },
  {
    path: 'createInstance',
    element: <CreateInstance />
  }
  , {
    path: 'desc',
    element: <Description />
  }
]);
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);


