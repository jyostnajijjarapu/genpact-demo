import React from 'react'
import './networking.css'
export default function Networking() {
    const [subnetchecked, setChecked] = React.useState('');
    const radioHandleChange = (event) => {
        setChecked(event);

    };
    return (
        <div>
            <div >
                <h3 style={{ backgroundColor: 'rgb(37, 165, 189)', color: 'white', padding: '10px', margin: '20px 0px 0px 0px' }}>Network Settings Details</h3>
                <div className='dataCard'>
                    <div className='subnet'>
                        <div>
                            <h4>VPC ID: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <select placeholder='Select VPC ID' className='inputfield'>
                                <option>

                                    vpc-405df129 (default)
                                </option>
                                <option>
                                    vpc-405df129 (default)
                                </option>
                            </select>
                        </div>
                        <div style={{ marginLeft: '10%', display: 'flex', alignItems: 'center' }}>
                            <input type="radio" name='vmarch' value='CreateSubnet' style={{ height: '25px', width: '25px', verticalAlign: 'middle' }} checked={subnetchecked === 'CreateSubnet'}
                                onChange={(event) => radioHandleChange(event.target.value)} /> <label style={{ fontSize: '20px' }}>Create Subnet</label>
                        </div>
                        <div style={{ marginLeft: '10%', display: 'flex', alignItems: 'center' }}>
                            <input type="radio" name='vmarch' value='ExistingSubnet' style={{ height: '25px', width: '25px', verticalAlign: 'middle' }} checked={subnetchecked === 'ExistingSubnet'}
                                onChange={(event) => radioHandleChange(event.target.value)} /> <label style={{ fontSize: '20px' }}>Select existing subnet</label>
                        </div>
                    </div>
                    <div>
                        {
                            (
                                (subnetchecked == 'CreateSubnet') ? (
                                    <div className='createsubnet'>
                                        <div>
                                            <h4>Subnet Name: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                                            <input type='text' placeholder='My subnet-1' className='inputfield' />
                                        </div>
                                        <div>
                                            <h4>CIDR block: </h4>
                                            <select placeholder='Select CIDR block' className='inputfield'>
                                                <option>

                                                    0.0.0.0/0
                                                </option>
                                                <option>
                                                    vpc-405df129 (default)
                                                </option>
                                            </select>
                                        </div>
                                        <div>
                                            <h4>Availability Zone: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                                            <select placeholder='No preference' className='inputfield'>
                                                <option>

                                                    Asia Pacific(Mumbai)/ap-south-2a
                                                </option>
                                                <option>
                                                    vpc-405df129 (default)
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                ) : (
                                    <div>
                                        <div>
                                            <h4>Subnet ID: <span style={{ color: 'red', fontSize: '24px' }}>*</span> </h4>
                                            <select placeholder='Select Subnet ID' className='inputfield'>
                                                <option>

                                                    0.0.0.0/0
                                                </option>
                                                <option>
                                                    vpc-405df129 (default)
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                )
                            )
                        }
                    </div>
                </div>

            </div>
        </div>
    )
}
