import React from 'react'
import './rightbar.css'
import Search from '@mui/icons-material/Search';
import { TableData } from './TableData'
import { useNavigate } from "react-router-dom"
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
export default function Rightbar(props) {
    const navigate = useNavigate()
    const username = localStorage.getItem('user')
    const [awschecked, setChecked] = React.useState(false);
    const [azurechecked, setChecked1] = React.useState(false);
    const [instancechecked, setChecked2] = React.useState('');
    const [instancechecked1, setChecked3] = React.useState('');
    const awsHandleChange = () => {
        setChecked(!awschecked);
    };
    const azureHandleChange = () => {
        setChecked1(!azurechecked);
    };
    const radioHandleChange = (event) => {
        setChecked2(event);
        console.log(event, "ec2")
    };

    const radio1HandleChange = (event) => {
        setChecked3(event);
    };
    const createHandle = () => {
        navigate('/createInstance')
    }
    const checkAll = (bx) => {
        // var cbs = document.getElementById('inputcheck');
        console.log('input', bx)
        // for (var i = 0; i < cbs.length; i++) {
        //     if (cbs[i].type == 'checkbox') {
        //         cbs[i].checked = bx.checked;
        //     }
        // }
    }
    return (
        <div className='rightbar'>
            <h1 style={{ color: 'GrayText' }}>Unified Portal</h1>
            <div className='rightbarCard'>
                <div className='cloudconsole'>
                    <div>
                        <h3 style={{ color: 'GrayText' }}>Cloud Console:</h3>
                    </div>

                    <div style={{ marginLeft: 10 }}>
                        <label>
                            <input type="checkbox" value='AWS' checked={awschecked}
                                onChange={awsHandleChange} />
                            AWS
                        </label>
                    </div>
                    <div style={{ marginLeft: 10 }}>
                        <label htmlFor="">
                            <input type="checkbox" value='Azure' checked={azurechecked}
                                onChange={azureHandleChange} />
                            Azure
                        </label>

                    </div>


                </div>
                <div>
                    {(awschecked == true && azurechecked == false) ?
                        (<div>
                            <div className='cloudconsole'>
                                <div>
                                    <h3 style={{ color: 'GrayText' }}>Cloud Instance:</h3>
                                </div>

                                <div style={{ marginLeft: 10 }}>
                                    <label>
                                        <input type="radio" value='EC2' checked={instancechecked === 'EC2'} name="instance"
                                            onChange={(event) => radioHandleChange(event.target.value)} />
                                        EC2
                                    </label>
                                </div>
                                <div style={{ marginLeft: 10 }}>
                                    <label>
                                        <input type="radio" value='RDS' checked={instancechecked === 'RDS'} name="instance"
                                            onChange={(event) => radioHandleChange(event.target.value)} />
                                        RDS
                                    </label>

                                </div>
                                <div style={{ marginLeft: 10 }}>
                                    <label htmlFor="">
                                        <input type="radio" value='S3' checked={instancechecked === 'S3'} name="instance"
                                            onChange={(event) => radioHandleChange(event.target.value)} />
                                        S3
                                    </label>

                                </div>

                            </div>
                        </div>) : null
                    }
                </div>
                <div>
                    {(azurechecked == true && awschecked == false) ?
                        (<div>
                            <div className='cloudconsole'>
                                <div>
                                    <h4>Cloud Instance:</h4>
                                </div>

                                <div style={{ marginLeft: 10 }}>
                                    <label>
                                        <input type="radio" value='VM' checked={instancechecked1 === 'VM'} name="instance"
                                            onChange={(event) => radio1HandleChange(event.target.value)} />
                                        VM
                                    </label>
                                </div>
                                <div style={{ marginLeft: 10 }}>
                                    <label htmlFor="">
                                        <input type="radio" value='SQL' checked={instancechecked1 === 'SQL'} name="instance"
                                            onChange={(event) => radio1HandleChange(event.target.value)} />
                                        SQL
                                    </label>

                                </div>
                                <div style={{ marginLeft: 10 }}>
                                    <label htmlFor="">
                                        <input type="radio" value='StorageAccount' checked={instancechecked1 === 'StorageAccount'} name="instance"
                                            onChange={(event) => radio1HandleChange(event.target.value)} />
                                        Storage Account
                                    </label>

                                </div>

                            </div>
                        </div>) : null
                    }
                </div>
                <div className='searchbarflex'>


                    <div style={{ flex: 10 }}>
                        {
                            (
                                (awschecked == true && instancechecked !== 'EC2') ? (
                                    <div className="searchbar">
                                        <Search className='searchIcon' />
                                        <input placeholder="Search here..." className="searchInput" />
                                    </div>

                                ) : null
                            )
                        }
                    </div>
                    <div style={{ flex: 2 }}>
                        {
                            ((username == 'L2 User' || username == 'L3 User') && awschecked == true && instancechecked === 'EC2') ?
                                (<button className='createBtn' onClick={createHandle}>Create Instance</button>) : null
                        }

                    </div>
                </div>
                <div>

                    {
                        ((username === 'L1 User' || username === 'L2 User' || username === 'L3 User') && awschecked == true && instancechecked !== 'EC2') ?

                            (
                                <div className='tableContainer'>
                                    <table className='table table-striped'>
                                        <thead>
                                            <tr>
                                                <th>RITM NO</th>
                                                <th>Requested Console</th>
                                                <th>Requested Instance</th>
                                                <th>No.Of Instances</th>
                                                <th>Requested On</th>
                                                <th>Created By</th>
                                                <th>Created For</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {TableData.map((item) => {
                                                return (
                                                    <tr>
                                                        <td>{item['RITMNo']}</td>
                                                        <td>{item.reqConsole}</td>
                                                        <td>{item.reqInstance}</td>
                                                        <td>{item.noOfInstances}</td>
                                                        <td>{item.reqOn}</td>
                                                        <td>{item.createdBy}</td>
                                                        <td>{item.createdFor}</td>
                                                        <td>{item.status}</td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            ) : null
                    }
                </div>
                <div>
                    {
                        ((username === 'L1 User' || username === 'L2 User' || username === 'L3 User') && awschecked == true && instancechecked === 'EC2') ?
                            (
                                <div>
                                    <h2 style={{ backgroundColor: '#e7e7e7', padding: '10px', color: 'GrayText', fontWeight: '500' }}>Instances(1/1)</h2>
                                    <div className="searchbar">
                                        <Search className='searchIcon' />
                                        <input placeholder="Find instance by attribute or tag(case-sensitive)" className="searchInput" />
                                    </div>
                                    <div style={{ marginTop: '20px' }}>
                                        <span className='instanceState'>Instance State=Running</span>
                                        <span className='instanceState'>X</span>
                                        <span className='instanceState1' >Clear Filters</span>
                                    </div>
                                    <div style={{ marginTop: '20px' }}>
                                        <table className='ec2table'>
                                            <thead>
                                                <tr>
                                                    <th><div ><input type="checkbox" onclick={checkAll(this)} id='inputcheck' /></div></th>
                                                    <th><div className='arrowfilter'>Name <ArrowDropDownIcon /></div></th>
                                                    <th>RITM ID</th>
                                                    <th>Instance ID</th>
                                                    <th><div className='arrowfilter'>Instance State<ArrowDropDownIcon /></div></th>
                                                    <th><div className='arrowfilter'>Instance Type<ArrowDropDownIcon /></div></th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tr>
                                                <td><input type="checkbox" onclick={checkAll(this)} id='inputcheck' /></td>
                                                <td>bieno-build-0</td>
                                                <td>RITM282765</td>
                                                <td>I-05cb89481b286c310</td>
                                                <td style={{ color: 'green' }}>Running</td>
                                                <td>t2micro</td>
                                                <td style={{ color: 'green' }}>checked</td>
                                            </tr>


                                        </table>
                                    </div>
                                </div>
                            ) : null

                    }
                </div>
            </div>
        </div>
    )
}
