export const TableData = [
    {
        RITMNo: 'RIT0000567890',
        reqConsole: "AWS",
        reqInstance: "EC2",
        noOfInstances: "01",
        reqOn: '27/07/2023',
        createdBy: 'Madhuri',
        createdFor: 'Jyostna',
        status: 'Approved'
    },
    {
        RITMNo: 'RIT0000567891',
        reqConsole: "AWS",
        reqInstance: "EC2",
        noOfInstances: "02",
        reqOn: '27/07/2023',
        createdBy: 'Madhuri',
        createdFor: 'Jyostna',
        status: 'Pending'
    },
    {
        RITMNo: 'RIT0000567892',
        reqConsole: "AWS",
        reqInstance: "EC2",
        noOfInstances: "02",
        reqOn: '27/07/2023',
        createdBy: 'Madhuri',
        createdFor: 'Jyostna',
        status: 'Rejected'
    },
    {
        RITMNo: 'RIT0000567893',
        reqConsole: "AWS",
        reqInstance: "EC2",
        noOfInstances: "02",
        reqOn: '27/07/2023',
        createdBy: 'Madhuri',
        createdFor: 'Jyostna',
        status: 'Approved'
    }, {
        RITMNo: 'RIT0000567894',
        reqConsole: "AWS",
        reqInstance: "EC2",
        noOfInstances: "02",
        reqOn: '27/07/2023',
        createdBy: 'Madhuri',
        createdFor: 'Jyostna',
        status: 'Rejected'
    },
]