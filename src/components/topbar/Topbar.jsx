import React from 'react'
import './topbar.css'
import { Person, Notifications } from '@mui/icons-material';

export default function Topbar(props) {
    console.log(props, "topbar")
    const user = localStorage.getItem('user')
    return (
        <div className='topbar'>
            <img src="/assets/Genpact.png" alt="" className='topbarImage' />
            <div className='topbarItems'>
                <div className="topbarIconItem">
                    <Notifications />
                    <span className="topbarIconBadge">1</span>
                </div>
                <div className="topbarIconItem">
                    <Person />
                    <span className="topbarIconBadge">1</span>
                </div>
                <div>{user}</div>
            </div>
        </div>
    )
}
