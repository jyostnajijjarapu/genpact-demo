import React from 'react'
import { ImagesData } from './ImagesData'
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
export default function Details() {
    const onImageClick = () => {

    }
    const [account, setAccount] = React.useState('');

    const handleChange = (event) => {
        setAccount(event.target.value);
    };
    return (
        <div>
            <div>
                <h3 style={{ backgroundColor: 'rgb(37, 165, 189)', color: 'white', padding: '10px', margin: '20px 0px 0px 0px' }}>Name and Instance Details</h3>
                <div className='detailsCard'>
                    <div className='row'>
                        <div>
                            <h4>RITM NO: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <input type='text' placeholder='Enter RITM number' className='inputfield' />
                        </div>
                        <div>
                            <h4>Account ID: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <FormControl fullWidth sx={{ minWidth: 300 }} size="small" className='inputfield'>
                                <InputLabel id="demo-simple-select-label">Select Account</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={account}
                                    label="Select Account"
                                    onChange={handleChange}
                                >
                                    <MenuItem value={10}>10</MenuItem>
                                    <MenuItem value={20}>20</MenuItem>
                                    <MenuItem value={30}>30</MenuItem>
                                </Select>
                            </FormControl>

                        </div>
                        <div>
                            <h4>Instance Name: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <input type='text' placeholder='Enter instance name' className='inputfield' />
                        </div>
                        <div>
                            <h4>Description: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <input type='text' placeholder='Enter description' className='inputfield' />
                        </div>

                    </div>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <h4>No.of Instances: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <input type='text' placeholder='Enter no.of instances' className='inputfield' />
                        </div>
                        <div style={{ marginLeft: '7%' }}>
                            <h4>Instance Type: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <select placeholder='Select instance type' className='inputfield' >
                                <option style={{ whiteSpace: 'pre-wrap', width: '92px' }}>
                                    t2.micro
                                    Family.t2 1vCPU 1GiB Memory Current Generation:true.
                                    On-Demand Linux Pricing:0.0124 USD per Hour
                                    On-Demand Window Pricing :0.017 USD per Hour

                                </option>
                                <option>
                                    t2.micro
                                    Family.t2 1vCPU 1GiB Memory Current Generation:true.
                                    On-Demand Linux Pricing:0.0124 USD per Hour
                                    On-Demand Window Pricing :0.017 USD per Hour
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h3 style={{ backgroundColor: 'rgb(37, 165, 189)', color: 'white', padding: '10px', margin: '20px 0px 0px 0px' }}>Amazon Machine Image (AMI)</h3>
                <div className='detailsCard'>
                    <div className='imageList'>
                        {ImagesData.map((val, key) => {
                            return (
                                <div className='imageCard'>
                                    <h2
                                        key={key}
                                        onClick={onImageClick}>{val.imgname}
                                    </h2>
                                    <img key={key} src={val.imgsrc} alt="logo" width={150} height={60} style={{ marginTop: '40px' }} />
                                </div>

                            )
                        })
                        }

                    </div>
                </div>

            </div>
            <div>
                <div className='amifields'>
                    <div>
                        <h4>Amazon Machine Image(AMI): <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                        <select placeholder='Select instance type' className='amiInputfield'>
                            <option>
                                Ubuntu server 22.04 LTS(HVM),SSD Volume Type

                            </option>
                            <option>
                                Windows server
                            </option>
                        </select>
                    </div>
                    <div>
                        <h4>AMI ID: </h4>
                        <input type='text' value={'ami-0f5ee92e2d6afc18'} className='inputfield' />
                    </div>
                    <div>
                        <h4>VM Architecture: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                        <div style={{ display: 'flex' }}>
                            <div>
                                <input type="radio" name='vmarch' /> <label>X64</label>
                            </div>
                            <div>
                                <input type="radio" name='vmarch' /> <label>Arm 64</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div style={{ display: 'flex' }}>
                    <div>
                        <h4>AMI Description: </h4>
                        <input type='text' value={'canonical,ubuntu,22.04 LTS,amd64 jammy image build on 2023-05-16'} className='amiInputfield' />
                    </div>
                    <div style={{ marginLeft: '23%' }}>
                        <h4>Root Device Type: </h4>
                        <input type='text' value={'ebs'} className='inputfield' />
                    </div>
                </div>
            </div>
            <div>
                <h3 style={{ backgroundColor: 'rgb(37, 165, 189)', color: 'white', padding: '10px', margin: '20px 0px 0px 0px' }}>Key Pair Details</h3>
                <div className='detailsCard'>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <h4>SSH Public key source: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <select placeholder='Select instance type' className='amiInputfield'>
                                <option>
                                    Use existing key pair

                                </option>
                                <option>
                                    generate new key pair
                                </option>
                            </select>
                        </div>
                        <div style={{ marginLeft: '5%' }}>
                            <h4>Key pair name: <span style={{ color: 'red', fontSize: '24px' }}>*</span></h4>
                            <select placeholder='Select key pair' className='amiInputfield'>
                                <option>
                                    use existing key pair

                                </option>
                                <option>
                                    Windows server
                                </option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div style={{ display: 'flex' }}>
                <div>
                    <h4>Key pair type: </h4>
                    <div style={{ display: 'flex' }}>
                        <div>
                            <input type="radio" name='vmarch' /> <label>RSA</label>
                        </div>
                        <div>
                            <input type="radio" name='vmarch' /> <label>ED25519</label>
                        </div>
                    </div>
                </div>
                <div style={{ marginLeft: '5%' }}>
                    <h4>private key file format: </h4>
                    <div style={{ display: 'flex' }}>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <input type="radio" name='vmarch' /> <label>.pem</label>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <input type="radio" name='vmarch' /> <label>.ppk</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
