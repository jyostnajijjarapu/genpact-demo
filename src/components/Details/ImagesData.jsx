export const ImagesData = [
    {
        imgname: 'Ubuntu',
        imgsrc: '/assets/ubuntu.png'
    },
    {
        imgname: 'Windows',
        imgsrc: '/assets/windows.png'
    },
    {
        imgname: 'Amazon Linux',
        imgsrc: '/assets/aws.png'
    },
    {
        imgname: 'macOS',
        imgsrc: '/assets/macOS.png'
    },
    {
        imgname: 'Red Hat',
        imgsrc: '/assets/redHat.png'
    }
]