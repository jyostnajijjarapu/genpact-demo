import React from 'react'
import './sidebar.css'
import { useNavigate } from "react-router-dom"

import { SidebarData } from './SidebarData'
export default function Sidebar() {
    const navigate = useNavigate()

    return (
        <div className='sidebar'>
            <ul className='sidebarlist'>
                {SidebarData.map((val, key) => {
                    return (
                        <li
                            key={key}
                            onClick={() => navigate(val.link)}
                            className='row'
                            id={window.location.pathname == val.link ? "active" : ""}>
                            <div className='icon'>{val.icon}</div>
                        </li>
                    )
                })}

            </ul>
        </div>
    )
}
