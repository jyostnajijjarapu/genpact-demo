import HomeIcon from '@mui/icons-material/Home';
import ImportantDevicesIcon from '@mui/icons-material/ImportantDevices';
import DescriptionIcon from '@mui/icons-material/Description';
export const SidebarData = [
    {
        icon: <HomeIcon fontSize='large' />,
        title: 'Home',
        link: '/home'
    },
    {
        icon: <ImportantDevicesIcon fontSize='large' />,
        title: 'Dashboard',
        link: '/dashboard'
    },
    {
        icon: <DescriptionIcon fontSize='large' />,
        title: 'Description',
        link: '/desc'
    },
]